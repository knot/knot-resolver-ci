# SPDX-License-Identifier: GPL-3.0-or-later


# These images are built in the declared order. The order is mostly just
# important for the images that have each other in the 'base_image' attribute,
# otherwise, it should not matter.

add_image 'debian11-base' 'debian-base' \
	'debian_version' '11'

add_image 'debian12-interim' 'debian-base'\
	'debian_version' '12' \
	'push_image' '0'
add_image 'debian12-base' 'debian-12-adds' \
	'base_image' "${image_tag['debian12-interim']}"

add_image 'debian12-knot_master' 'knot' \
	'base_image' "${image_tag['debian12-base']}" \
	'knot_branch' 'master' \
	'special_arg' '--no-cache' \
	'is_nightly' '1'

add_image 'debian11-knot_3_2' 'knot' \
	'base_image' "${image_tag['debian11-base']}" \
	'knot_branch' '3.2'
add_image 'debian11-knot_3_1' 'knot' \
	'base_image' "${image_tag['debian11-base']}" \
	'knot_branch' '3.1'
add_image 'debian12-knot_3_3' 'knot' \
	'base_image' "${image_tag['debian12-base']}" \
	'knot_branch' '3.3'
add_image 'debian12-knot_3_2' 'knot' \
	'base_image' "${image_tag['debian12-base']}" \
	'knot_branch' '3.2'

add_image 'coverity' 'debian-coverity' \
	'base_image' "${image_tag['debian12-knot_3_3']}" \
	'special_arg' '--secret id=coverity-token,env=COVERITY_SCAN_TOKEN' \
	'coverity_scan_project_name' "$COVERITY_SCAN_PROJECT_NAME"

add_image 'arch' 'arch' \
	'special_arg' '--no-cache' \
	'is_nightly' '1'

add_image 'manager' 'manager'
