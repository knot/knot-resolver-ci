#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later

source "$(dirname "${0}")/ci-env.inc.sh"

section_start ()
{
	echo -e "\e[0Ksection_start:$(date +%s):ci_image_$i[collapsed=true]\r\e[0KImage '$tag'"
}

section_end ()
{
	echo -e "\e[0Ksection_end:$(date +%s):ci_image_$i\r\e[0K"
}

failed_images=()

exit_code=0
i=0
for repo in "${repos[@]}"; do
	i=$((i+1))
	err=0

	tag="${image_tag["$repo"]}"
	section_start

	if [ "${KRES_IS_NIGHTLY:-0}" = "1" -a "${is_nightly["$repo"]:-0}" != "1" ]; then
		ci_log "Skipping '${image_tag["$repo"]}' because we are doing a nightly rebuild"
		section_end
		continue
	fi

	ci_log "Building '$tag'"
	build_args=()
	build_args+=(${special_arg["$repo"]:-})
	if [ -n "${base_image["$repo"]:-}" ]; then
		build_args+=("--build-arg" "KRES_BASE_IMAGE=${base_image["$repo"]}")
	fi
	if [ -n "${knot_branch["$repo"]:-}" ]; then
		build_args+=("--build-arg" "KNOT_BRANCH=${knot_branch["$repo"]}")
	fi
	if [ -n "${debian_version["$repo"]:-}" ]; then
		build_args+=("--build-arg" "KRES_DEBIAN_VERSION=${debian_version["$repo"]}")
	fi
	if [ -n "${coverity_scan_project_name["$repo"]:-}" ]; then
		build_args+=("--build-arg" "COVERITY_SCAN_PROJECT_NAME=${coverity_scan_project_name["$repo"]}")
	fi

	dump_image_info "$repo"
	ci_log "Build args: ${build_args[*]}"

	set +e
	"$docker_cmd" build \
		"${build_args[@]}" \
		--cache-from "${image_name["$repo"]}:main" \
		--cache-from "${image_tag["$repo"]}" \
		--file "images/${dockerfile_dir["$repo"]}/Dockerfile" \
		--tag "${image_tag["$repo"]}" \
		.
	if [ "$?" -ne "0" ]; then
		failed_images+=("${image_tag["$repo"]}")
		exit_code=16
		set -e
		section_end
		continue
	fi
	set -e

	if [ "${push_image["$repo"]}" = "1" ]; then
		"$docker_cmd" push "${image_tag["$repo"]}"
	fi

	if [ "$err" -eq "0" ]; then
		ci_log "Finished '${image_tag["$repo"]}' - [OK]"
	else
		ci_log "Finished '${image_tag["$repo"]}' - [ERROR]"
	fi
	section_end
done

if [ "$exit_code" -ne "0" ]; then
	ci_log "Finished with errors in the following images:"
	for img in "${failed_images[@]}"; do
		ci_log " - $img"
	done
fi
exit $exit_code
