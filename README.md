# Knot Resolver CI

This repository is used to manage infrastructure data used by
[Knot Resolver](https://gitlab.nic.cz/knot/knot-resolver) (the main repository)
CI/CD pipelines.


## Why?

This gives us the ability to safely iterate on changes to our CI images.

We used to have a single version of the CI images globally to build everything.
This meant that if we wanted to make changes to the images, we would always face
the fear of breaking the build for all Knot Resolver branches, without even a
clear path to go back to a working state. We also used to build and push the
images from our local machines kind of asystematically, meaning that the images
would not necessarily be the ones described by the `Dockerfile`s in any
particular Git branch.

This repository fixes the situation by building the images centrally in the CI
and properly versioning them based on Git branches and, more importantly, Git
tags. Each build in the main repository then points to a specific version of the
images in this one, allowing us to make changes local to specific branches,
without the fear of breaking builds for others. This hopefully allows us to make
more frequent updates to our CI infrastructure, update the authoritative-only
Knot DNS to new versions (even `master`) etc.


## How does this work?

Each commit in this repository triggers a CI/CD pipeline that builds images used
by the main repository to build and test Knot Resolver. Each image has a tag in
the format `:<ref>`, where `<ref>` is the name of the Git branch or Git tag in
this repository.

In "production", we use images that came from Git tags (this is configured by
the [`IMAGE_TAG` variable in `.gitlab-ci.yml`](https://gitlab.nic.cz/knot/knot-resolver/-/blob/f1e737f782cbac2fb149a77abc13b291d25d853f/.gitlab-ci.yml#L23)
in the main repository). Images that came from Git branches are used for
development - i.e. when making changes to the images.

Tagged images should never change, with the notable exception of those whose
`is_nightly` option in `ci-image-defs.sh` is set to `1` - those are
[rebuilt nightly](https://gitlab.nic.cz/knot/knot-resolver-ci/-/pipeline_schedules)
to keep up with Knot DNS's development and check that Knot Resolver works with
their newest version.


## Repository structure

* `images`: contains the `Dockerfile`s defining our CI images, as well as the
  shell scripts used to build them. See `.gitlab-ci.yml` for info on how the
  scripts are run.

  * `ci-make.sh`: the script that builds the images

  * `ci-env.inc.sh`: contains definitions and globals for the `ci-make.sh` script.

  * `ci-image-defs.inc.sh`: defines all of our images, their names, tags,
    hierarchy, and other options

* `libfaketime`: a subtree from `https://github.com/wolfcw/libfaketime.git`,
  containing a version of faketime that works with Python 3.11

  * Use `git subtree --prefix libfaketime ...` to manipulate (see `man
    git-subtree`)


## How do I make changes?

* Create a new branch *here* and in the
  *[main repository](https://gitlab.nic.cz/knot/knot-resolver)*

* Make your changes *here*, push, wait for the CI to finish

* In `.gitlab-ci.yml` in the *main repository*, set `IMAGE_TAG` to the name of
  your **branch** *here*, push, and check the result

* While there is a problem:

  * Adjust your changes *here*, push, wait for the CI to finish

  * Re-run the pipeline in the *main repository*

  * Repeat until the problem is fixed

* Merge your changes here to `main`

* Tag the latest commit in `main`

  * A good tag name is in the date format `vYYYYMMDD`, e.g. `v20240424`. If
    there are multiple tags in a day, add a `-N` suffix, where `N` is a decimal
    integer; start from `1`

* Add a [schedule](https://gitlab.nic.cz/knot/knot-resolver-ci/-/pipeline_schedules)
  to build the image containing Knot DNS `master` branch

  * Set interval pattern to `16 23 * * *`

  * Set `KRES_IS_NIGHTLY` to `1`

  * Optionally, remove the old schedule, if the old tag remains unused

* In `.gitlab-ci.yml` in the *main repository*, set the `IMAGE_TAG` variable to
  the name of your **tag** *here*

  * The `ci-image-is-tag` job in the *main repository* sanity-checks this - make
    sure it succeeds

* Push the changed `.gitlab-ci.yml` to the *main repository* and check the
  result for the last time

* Profit!
